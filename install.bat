@ECHO OFF
REG ADD "HKCR\Folder\shell\Set as Desktop Folder" /f
REG ADD "HKCR\Folder\shell\Set as Desktop Folder\command" /d "\"%CD%\DesktopSwitcher.exe\"  \"%%1\"" /f

REG ADD "HKCR\DesktopBackground\Shell\Reset Desktop Folder" /f
REG ADD "HKCR\DesktopBackground\Shell\Reset Desktop Folder\command" /d "\"%CD%\DesktopSwitcher.exe\"" /f
cls
