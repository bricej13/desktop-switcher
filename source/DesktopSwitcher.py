from win32com.shell import shell, shellcon
import sys, os
def setDesktopFolder(directory):
    try:
        # Set new folder path
        shell.SHSetFolderPath(shellcon.CSIDL_DESKTOP, directory, None)
        # Notify explorer.exe that a change has been made
        shell.SHChangeNotify(shellcon.SHCNE_ASSOCCHANGED, shellcon.SHCNF_FLUSHNOWAIT, None, None)
        return True
    except:
        return False

if __name__ == "__main__":
    try:
        defaultFolder = sys.argv[1]
        if setDesktopFolder(sys.argv[1]):
            print "Desktop set to " + sys.argv[1]
    except Exception:
        # Reset folder to default
        defaultFolder = os.getenv("USERPROFILE") + "\\Desktop"
        print setDesktopFolder(defaultFolder)
        print "Desktop set to " + defaultFolder	
