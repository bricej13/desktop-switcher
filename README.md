# The Desktop Switcher
What do you actually use your desktop for? Do you us it as a scratch area to store temporary files? Do you have a million files on it or zero?

The Desktop Switcher makes the desktop useful. Just right-click on any folder to set it as the desktop folder.

![The Desktop Switcher](https://bitbucket.org/bricej13/desktop-switcher/raw/9071b79f885f3e96432186903116daf0b36104fc/screenshot.png)

But what happens when I want my desktop folder back? Easy. Right-click on your desktop and choose 'Reset Desktop Folder'

![The Desktop Switcher](https://bitbucket.org/bricej13/desktop-switcher/raw/fac6a8e59c12560664b41122b2be682e3f9bd572/screenshotDesktop.png)

## Installation
* Install the latest version of [pywin32](http://sourceforge.net/projects/pywin32/files/pywin32/)
* Download source files (Download link is on the right)
* Unzip files where you want the program to run from
* run install.bat

## How to use
* Right-click on any folder and choose 'Set as Desktop Folder'
* Right-click on the desktop to 'Reset Desktop Folder' to the normal desktop folder

## Uninstall
* run uninstall.bat
* delete files